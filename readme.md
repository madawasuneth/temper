##Setup development environment

clone repository 

	git clone https://madawasuneth@bitbucket.org/madawasuneth/temper.git	

## Backend server(API) setup


	cd server
	
	install dependancies
	
	composer install
	
	cp .env.example .env
	
	configure db connection
	
	DB_CONNECTION=mysql
	DB_HOST=127.0.0.1
	DB_PORT=3306
	DB_DATABASE=temper
	DB_USERNAME=root
	DB_PASSWORD=Password@123
	
	then add write permissions to following directories
	
	chmod -R 777 storage/ bootstrap/cache/
	
	create database in yout mysql server
	
	then run db migrations & seeders
	
	php artisan migrate & php artisan db:seed	
	
	then run
	
	php artisan serve --port=1111
	
	goto http://localhost:1111
	
	
## Client setup

	cd client
	
	install dependancies
	
	npm install 
	
	then run
	
	npm run dev
	
	goto http://localhost:8080
	
	
	Default Admin Account email: abc@testmail.com, password: Password@123
	Default System User Account email: def@testmail.com, password: Password@123
	
