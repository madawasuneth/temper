/* eslint-disable */
const axios = require('axios')
import auth from '../session/auth'
import {
    router
} from '../main';

export default {

    request: function (url, method, data) {
        return new Promise((resolve, reject) => {
            const options = {
                baseURL: 'http://localhost:1111/api/',
                url: url,
                method: method,
                responseType: 'json',
                headers: {
                    'Authorization': auth.getAuthToken()
                }
            }

            if (method.toLowerCase() !== "get") {
                options.data = data
            }

            axios(options).then(function (response) {
                resolve(response.data);
            }).catch(function (error) {
                //handle error
                switch (error.response.status) {
                    case 401: // if unauthorized requet return login
                        router.push({
                            name: 'login'
                        })
                        break;
                    default:
                        return reject(response);
                }
            });
        });
    }
}
