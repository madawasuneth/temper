import Vue from 'vue'
import Router from 'vue-router'
import auth from '@/session/auth';
import Dashboard from '@/components/Dashboard.vue';
import Home from '@/components/Home.vue';
import Login from '@/components/Login.vue';
import PageNotFound from '@/components/PageNotFound.vue';
import PermissionDenied from '@/components/PermissionDenied.vue';

Vue.use(Router);

auth.checkAuth();

export default new Router({
    routes: [{
        path: '/',
        name: 'home',
        component: Home
    }, {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            auth: false
        }
    }, {
        path: '/dashboard',
        name: 'dashboard',
        component: Dashboard,
        meta: {
            auth: true
        }
    }, {
        path: '/page-not-found',
        name: 'page-not-found',
        component: PageNotFound,
        meta: {
            auth: false
        }
    }, {
        path: '/permission-denied',
        name: 'permission-denied',
        component: PermissionDenied,
        meta: {
            auth: false
        }
    }, { path: '*', redirect: '/page-not-found' }]
})
