<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\ServiceInterface;
use App\Services\UserApplicationService;
use Illuminate\Support\Facades\DB;
use App\UserApplication;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ServiceInterface::class, function () {
            return new UserApplicationService(new UserApplication(), new DB(), config('application'));
        });
    }
}
