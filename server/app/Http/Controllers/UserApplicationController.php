<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ServiceInterface;

class UserApplicationController extends Controller
{

    private $userAppService;

    public function __construct(ServiceInterface $userAppService)
    {
        $this->userAppService = $userAppService;
    }

    public function getWeeklyCohorts()
    {
        $weeklyCohortsData = $this->userAppService->getWeeklyCohorts();

        if($weeklyCohortsData['error']) {
            return $this->error(500, null, 'error occurred during while retrieving chart data.');
        }

        return $this->success(200, $weeklyCohortsData['data']);
    }
}
