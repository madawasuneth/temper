<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * success response
     * 
    * Sample input:
    *
    * $data = [
    *     'status' => 200,//required,
    *     'data' => [
    *         'name' => 'Jhone Smith'
    *     ],
    *     'message' => 'Successfully Created'
    * ]
    */
    protected function success($status, $data = [], $message = 'success')
    {
        return [
            'error' => false,
            'data' => $data,
            'message' => $message
        ];

        return response()->json($data, $status);
    }

    /**
     * error response
     * 
    * Sample input:
    *
    * $data = [
    *     'status' => 500,
    *     'data' => [],
    *     'message' => 'Error occured while creating user.'
    * ]
    */
    protected function error($status, $data, $message = 'error')
    {
        $response = [
            'error' => true,
            'data' => $data,
            'message' => $message
        ];

        return response()->json($data, $status);
    }
}
