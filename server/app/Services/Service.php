<?php

namespace App\Services;

class Service
{
    protected function success($data = [], $message = 'success')
    {
        return [
            'error' => false,
            'data' => $data,
            'message' => $message
        ];
    }

    protected function error($data = [], $message = 'error')
    {
        return [
            'error' => true,
            'data' => $data,
            'message' => $message
        ];
    }
}