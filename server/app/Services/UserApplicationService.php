<?php

namespace App\Services;

use App\Services\ServiceInterface;
use App\UserApplication;
use Illuminate\Support\Facades\DB;

class UserApplicationService extends Service implements ServiceInterface
{

    private $model;

    private $db;

    private $config;

    public function __construct(UserApplication $model, DB $db, $config = [])
    {
        $this->model = $model;
        $this->db = $db;
        $this->config = $config;
    }

    public function getWeeklyCohorts()
    {
        try {

            $chartData = [];

            $onboarduserPercentages = $this->db::select('select YEARWEEK(created_at) as yearweek, onboarding_percentage, count(*)  as onboard_users from user_applications group by YEARWEEK(created_at), onboarding_percentage order by yearweek, onboarding_percentage');

            $weeklyOnboardCount = $this->db::select('select count(*)  as onboardusers, YEARWEEK(created_at) as yearweek from user_applications group by YEARWEEK(created_at)');

            $weeklyUserPercentages = $this->getPercentageWiseWeeklyOnboardUsers($onboarduserPercentages);
            
            $totalweeklyOnboardUsers = $this->getTotalweeklyOnboardUsers($weeklyOnboardCount);

            foreach ($weeklyUserPercentages as $week => $userPercentage) {

                $weeklyDataSet = $this->genarateWeeklyDataSet($userPercentage);
                
                $percentages = $this->calculatePercentage($weeklyDataSet, $totalweeklyOnboardUsers[$week]);

                $chartData[] = [
                    'name' => $week,
                    'data' => array_values($percentages)
                ];
            }

            return $this->success($chartData);

        } catch (\Exception $e) {
            echo 'Message: ' . $e->getMessage();
            return $this->error(null, 'error occurred while retrieving data.');
        }
    }

    /**
     * get percentage wise weekly onboard users
     */
    protected function getPercentageWiseWeeklyOnboardUsers($onboarduserPercentages)
    {
        $weeklyUserPercentages = [];

        $validPercentages = array_keys($this->config["onboard_percentage_steps"]);
        $defaultPercentage = array_fill_keys($validPercentages, 0);
        
        foreach ($onboarduserPercentages as $onboarduserPercentage) {
            if (!in_array($onboarduserPercentage->onboarding_percentage, $validPercentages)) { // given csv file contain invalid percentages
                continue;
            }
            $weeklyUserPercentages[$onboarduserPercentage->yearweek][$onboarduserPercentage->onboarding_percentage] = $onboarduserPercentage->onboard_users;

        }

        return $weeklyUserPercentages;
    }

    /**
     * weekly data set
     */
    protected function genarateWeeklyDataSet($onboarduserPercentags)
    {
        $validPercentages = $this->config["onboard_percentage_steps"];

        $defaultPercentage = array_fill_keys($validPercentages, 0);

        foreach ($onboarduserPercentags as $key => $value) {
            if (!$step = array_search($key, array_keys($validPercentages))) {
                continue;
            }
            $defaultPercentage[$step] = $value;
        }

        return $defaultPercentage;
    }

    /**
     * get weekly onboard users
     */
    protected function getTotalweeklyOnboardUsers($weeklyOnboardCount)
    {
        $totalweeklyOnboardArray = [];
        foreach ($weeklyOnboardCount as $value) {
            $totalweeklyOnboardArray[$value->yearweek] = $value->onboardusers;
        }
        return $totalweeklyOnboardArray;
    }

    /**
     * calculate weekly step wise percentage
     */
    protected function calculatePercentage($weeklyDataSet, $totalweeklyOnboardUsers) 
    {   
        $stepPercentages = [];
        $userSum = 0;

        foreach ($weeklyDataSet as $step => $numberOfUsers) {
            // if step 1 set percentage as 100 otherwise calculate percentage
            $stepPercentages[$step] = ($step == 1) ? 100 : (($totalweeklyOnboardUsers - $userSum) / $totalweeklyOnboardUsers ) * 100;
            $userSum += $numberOfUsers;
        }

        return $stepPercentages;
    }


}