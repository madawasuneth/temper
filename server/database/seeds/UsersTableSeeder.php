<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create admin account
        User::create([
            'id' => 1,
            'name' => 'Jhone Smith',
            'email' => 'abc@testmail.com',
            'password' => bcrypt('Password@123'),
            'role_id' => 1
        ]);

        error_log("Admin Account email: abc@testmail.com, password: Password@123");

        // create normal user account
        User::create([
            'id' => 2,
            'name' => 'Jeamy Adams',
            'email' => 'def@testmail.com',
            'password' => bcrypt('Password@123'),
            'role_id' => 2
        ]);

        error_log("System User Account email: def@testmail.com, password: Password@123");
    }
}
