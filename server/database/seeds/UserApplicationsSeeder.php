<?php

use Illuminate\Database\Seeder;
use App\UserApplication;

class UserApplicationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            $this->importCsv('export.csv');
        } catch(\Exception $e) {
            echo 'Error occoured while importing data, ' . $e->getMessage();
        }
    }

    /**
     * read csv file and return as an array
     */
    private function csvToArray($file, $delimiter = ';')
    {
        if (!file_exists($file) || !is_readable($file))
            return false;
    
        $header = null;
        $data = [];
        if (($handle = fopen($file, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header) {
                    $header = $row;
                } else {
                    $rowData = array_combine($header, $row);
                    $data[] = [
                        'user_id' => $rowData['user_id'],
                        'created_at' => $rowData['created_at'],
                        'onboarding_percentage' => ($rowData['onboarding_perentage']) ? $rowData['onboarding_perentage'] : 0, 
                        'application_count' => (is_numeric($rowData['count_applications'])) ? $rowData['count_applications'] : 0,
                        'accepted_application_count' => (is_numeric($rowData['count_accepted_applications'])) ? $rowData['count_accepted_applications'] : 0
                    ];
                }
            }
            fclose($handle);
        }
    
        return $data;
    }

    /**
     * Insert data to database
     */
    private function importCsv($file)
    {
        $filePath = public_path($file);

        $csvDataSet = $this->csvToArray($filePath);

        foreach ($csvDataSet as $row) {
            UserApplication::firstOrCreate($row);
        }

        return true;
    }
}
