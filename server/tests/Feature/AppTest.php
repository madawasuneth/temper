<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AppTest extends TestCase
{
    /**
     * A basic test.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/api');

        $response->assertStatus(401);
        $response->assertJson(['errors' => ['status' => 401, 'message' => 'Unauthenticated']], true);
    }
}
