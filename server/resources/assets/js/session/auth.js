import adapter from '../api/adapter';
import {router} from '../app';

const SESSION_KEY = 'user-session'; // local-storage key for save session

/**
 * This module handle the user session
 */
export default {
    SESSION_KEY: SESSION_KEY,
    meta: {
        id: '',
        name: '',
        token: '',
        role_id: '',
        email: ''
    },
    authenticated: false, // Authentication status of the logged in user
    user: {}, // user session data

    login (credentials) {
        var _this = this;
        return adapter.request('auth/login', 'post', credentials).then(function (response) {
            _this.createSession(response);
            return;
        })
        .catch(function (error) {
            // handle error
            console.log('error', error);
        });
    },
    
    createSession (response) {
        var session = response.data.data;
        this.user = {
            id: session.user.id,
            name: session.user.name,
            token: session.token,
            role_id: session.user.role_id,
            email: session.user.email
        };
        // Store session in the local storage
        localStorage.setItem(SESSION_KEY, JSON.stringify(this.user));
        this.authenticated = true;
        return;
    },

    // Remove the token when logout
    logout (redirect) {
        var _this = this;
        return adapter.request('auth/logout', 'post', {}).then(function (response) {
            localStorage.removeItem(SESSION_KEY);
            _this.authenticated = false;
            _this.user = {};
            // Redirect to given route
            if (redirect) {
                router.push({ path: redirect });
            }
            return;
        })
        .catch(function (error) {
            console.log('error', error);
        });
    },

    // Update session data by retrieve session from the localstorage
    // getAuthenticatedUser () {
    //     var sessionJson = localStorage.getItem(SESSION_KEY);
    //     var session = JSON.parse(sessionJson);
    //     if (session && session.token) {
    //         this.user = {
    //             id: session.user.id,
    //             name: session.user.name,
    //             token: session.token,
    //             role_id: session.user.role_id,
    //             email: session.user.email
    //         };
    //     } else {
    //         this.user = {};
    //     }
    //     return this.user
    // },

    // isAuthenticated () {
    //     var sessionJson = localStorage.getItem(SESSION_KEY);
    //     var session = JSON.parse(sessionJson);
    //     if (session && session.token) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // },

    checkAuth () {
        var sessionJson = localStorage.getItem(SESSION_KEY);
        var session = JSON.parse(sessionJson);
        if (session && session.token) {
            this.authenticated = true;
            this.user = {
                id: session.id,
                name: session.name,
                token: session.token,
                role_id: session.role_id,
                email: session.email
            };
        } else {
            this.authenticated = false;
            this.user = {};
        }
        
    },

    // Refresh Token
    refreshToken ($newToken) {
        this.user.token = $newToken;
        localStorage.setItem(SESSION_KEY, JSON.stringify(this.user));
    },

    // Return auth token
    getAuthToken () {
        var sessionJson = localStorage.getItem(SESSION_KEY);
        var session = JSON.parse(sessionJson);
        if (session && session.token) {
            return 'Bearer ' + session.token;
        } 
        return;
    }    
}